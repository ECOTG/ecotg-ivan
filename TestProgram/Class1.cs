﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Controller.Controllers;
using ECOTG.Model.Entity;
using System.Windows.Forms;
using ECOTG.Model.Data;

namespace TestProgram
{
    public class Program
    {
        public static void Main(string[] args)
        {
            AddConfig();
            


        }

        private static void AddConfig()
        {
            var config = new config
            {
                configId = 2,
                configName = "Test"
            };

            ConfigEntity configEntity = new ConfigEntity();
            
            
            String result = ConfigController.FormCrudService.Add(config, true);

            Console.WriteLine(result);
            foreach (var allConfig in configEntity.GetAllEntities())
            {
                Console.WriteLine(allConfig.configName);
            }

        }

        private static void EditConfig()
        {
            var config = ConfigController.FormCrudService.GetEntity(2);
            Console.WriteLine(config.configName);
        }

    }
}
