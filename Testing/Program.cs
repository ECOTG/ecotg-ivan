﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Controller.Controllers;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace Testing
{
    class Program
    {
        public static void Main(string[] args)
        {
            DeleteConfig();
            GetConfig();
            Console.ReadLine();

        }

        private static void AddConfig()
        {
            var config = new config
            {
                configId = 3,
                configName = "Config"
            };
            


            String result = ConfigController.FormCrudService.Add(config, true);

            Console.WriteLine(result);

        }

        private static void EditConfig()
        {
            var config = ConfigController.FormCrudService.GetEntity(2);
            config.configName = "Novi is pinoy";
            Console.WriteLine(ConfigController.FormCrudService.Edit(config, true));


        }

        private static void GetConfig()
        {

            ConfigEntity configEntity = new ConfigEntity();
            foreach (var allConfig in configEntity.GetAllEntities())
            {
                Console.WriteLine(allConfig.configName);
            }
        }

        private static void DeleteConfig()
        {
            ConfigEntity configEntity = new ConfigEntity();

            var config = ConfigController.FormCrudService.GetEntity(3);
            configEntity.Delete(config);

        }

    }
}
