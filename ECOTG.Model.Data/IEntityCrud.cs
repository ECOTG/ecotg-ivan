﻿using System.Collections.Generic;

namespace ECOTG.Model.Data
{
    public interface IEntityCrud<T>
    {
        IEnumerable<T> GetAllEntities();
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
