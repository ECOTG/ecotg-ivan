﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class ClaimStatusEntity : IEntityCrudWithIntegerKey<claimstatus>
    {
        public claimstatus GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.claimstatus.FirstOrDefault(e => e.claimStatusId == id);
            }
        }

        public IEnumerable<claimstatus> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.claimstatus select e;
                return entities.ToList();
            }
        }

        public void Insert(claimstatus entity)
        {
            using (var db = new ecotgEntities())
            {
                db.claimstatus.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(claimstatus entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(claimstatus entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.claimstatus.Remove(entity);
                db.SaveChanges();
            }
        }

    }
}
