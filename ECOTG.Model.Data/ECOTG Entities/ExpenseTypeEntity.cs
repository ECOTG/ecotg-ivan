﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class ExpenseTypeEntity : IEntityCrudWithIntegerKey<expensetype>
    {
        public expensetype GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.expensetype.FirstOrDefault(e => e.expenseTypeId == id);
            }
        }

        public IEnumerable<expensetype> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.expensetype select e;
                return entities.ToList();
            }
        }

        public void Insert(expensetype entity)
        {
            using (var db = new ecotgEntities())
            {
                db.expensetype.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(expensetype entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(expensetype entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.expensetype.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
