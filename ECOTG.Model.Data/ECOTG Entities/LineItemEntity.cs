﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class LineItemEntity : IEntityCrudWithIntegerKey<lineitem>
    {
        public lineitem GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.lineitem.FirstOrDefault(e => e.lineItemId == id);
            }
        }

        public IEnumerable<lineitem> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.lineitem select e;
                return entities.ToList();
            }
        }

        public void Insert(lineitem entity)
        {
            using (var db = new ecotgEntities())
            {
                db.lineitem.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(lineitem entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(lineitem entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.lineitem.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
