﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class ClaimEntity : IEntityCrudWithStringKey<claim>
    {
        public claim GetEntity (string id)
        {
            using (var db = new ecotgEntities())
            {
                return db.claim.FirstOrDefault(e => e.claimId == id);
            }
        }

        public IEnumerable<claim> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.claim select e;
                return entities.ToList();
            }
        }

        public void Insert(claim entity)
        {
            using (var db = new ecotgEntities())
            {
                db.claim.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(claim entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(claim entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.claim.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
