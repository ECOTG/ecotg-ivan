﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class AccountCodeEntity : IEntityCrudWithStringKey<accountcode>
    {
        public accountcode GetEntity(string id)
        {
            using (var db = new ecotgEntities())
            {
                return db.accountcode.FirstOrDefault(e => e.accountCodeId == id);
            }
        }

        public IEnumerable<accountcode> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.accountcode select e;
                return entities.ToList();
            }
        }

        public void Insert(accountcode entity)
        {
            using (var db = new ecotgEntities())
            {
                db.accountcode.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(accountcode entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(accountcode entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.accountcode.Remove(entity); 
                db.SaveChanges();
            }
        }
    }
}
