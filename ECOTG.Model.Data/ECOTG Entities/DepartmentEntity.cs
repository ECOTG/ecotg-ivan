﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class DepartmentEntity : IEntityCrudWithIntegerKey<department>
    {
        public department GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.department.FirstOrDefault(e => e.departmentId == id);
            }
        }

        public IEnumerable<department> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.department select e;
                return entities.ToList();
            }
        }

        public void Insert(department entity)
        {
            using (var db = new ecotgEntities())
            {
                db.department.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(department entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(department entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.department.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
