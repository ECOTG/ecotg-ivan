﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class BatchStatusEntity : IEntityCrudWithIntegerKey<batchstatus>
    {
        public batchstatus GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.batchstatus.FirstOrDefault(e => e.batchStatusId == id);
            }
        }

        public IEnumerable<batchstatus> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.batchstatus select e;
                return entities.ToList();
            }
        }

        public void Insert(batchstatus entity)
        {
            using (var db = new ecotgEntities())
            {
                db.batchstatus.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(batchstatus entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(batchstatus entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.batchstatus.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
