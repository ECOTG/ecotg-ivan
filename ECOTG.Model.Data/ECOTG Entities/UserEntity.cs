﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class UserEntity : IEntityCrudWithStringKey<user>
    {
        public user GetEntity(string id)
        {
            using (var db = new ecotgEntities())
            {
                return db.user.FirstOrDefault(e => e.userId == id);
            }
        }

        public IEnumerable<user> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.user select e;
                return entities.ToList();
            }
        }

        public void Insert(user entity)
        {
            using (var db = new ecotgEntities())
            {
                db.user.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(user entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(user entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.user.Remove(entity);
                db.SaveChanges();
            }
        }
              
    }
}
