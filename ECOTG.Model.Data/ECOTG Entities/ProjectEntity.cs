﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class ProjectEntity : IEntityCrudWithStringKey<project>
    {
        public project GetEntity(string id)
        {
            using (var db = new ecotgEntities())
            {
                return db.project.FirstOrDefault(e => e.projectId == id);
            }
        }

        public IEnumerable<project> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.project select e;
                return entities.ToList();
            }
        }

        public void Insert(project entity)
        {
            using (var db = new ecotgEntities())
            {
                db.project.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(project entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(project entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.project.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
