﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class refreshmentdetailsDetailsEntity : IEntityCrudWithIntegerKey<refreshmentdetails>
    {
        public refreshmentdetails GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.refreshmentdetails.FirstOrDefault(e => e.refreshmentDetailsId == id);
            }
        }

        public IEnumerable<refreshmentdetails> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.refreshmentdetails select e;
                return entities.ToList();
            }
        }

        public void Insert(refreshmentdetails entity)
        {
            using (var db = new ecotgEntities())
            {
                db.refreshmentdetails.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(refreshmentdetails entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(refreshmentdetails entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.refreshmentdetails.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
