﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class CostCentreEntity : IEntityCrudWithIntegerKey<costcentre>
    {
        public costcentre GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.costcentre.FirstOrDefault(e => e.costCentreId == id);
            }
        }

        public IEnumerable<costcentre> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.costcentre select e;
                return entities.ToList();
            }
        }

        public void Insert(costcentre entity)
        {
            using (var db = new ecotgEntities())
            {
                db.costcentre.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(costcentre entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(costcentre entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.costcentre.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
