﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class RefreshmentEntity : IEntityCrudWithIntegerKey<refreshment>
    {
        public refreshment GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.refreshment.FirstOrDefault(e => e.refreshmentId == id);
            }
        }

        public IEnumerable<refreshment> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.refreshment select e;
                return entities.ToList();
            }
        }

        public void Insert(refreshment entity)
        {
            using (var db = new ecotgEntities())
            {
                db.refreshment.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(refreshment entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(refreshment entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.refreshment.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
