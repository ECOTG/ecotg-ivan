﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class AuditTrailEntity : IEntityCrudWithIntegerKey<audittrail>
    {
        public audittrail GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.audittrail.FirstOrDefault(e => e.auditTrailId == id);
            }
        }

        public IEnumerable<audittrail> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.audittrail select e;
                return entities.ToList();
            }
        }

        public void Insert(audittrail entity)
        {
            using (var db = new ecotgEntities())
            {
                db.audittrail.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(audittrail entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(audittrail entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.audittrail.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
