﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class InvoiceEntity : IEntityCrudWithIntegerKey<invoice>
    {
        public invoice GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.invoice.FirstOrDefault(e => e.invoiceId == id);
            }
        }

        public IEnumerable<invoice> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.invoice select e;
                return entities.ToList();
            }
        }

        public void Insert(invoice entity)
        {
            using (var db = new ecotgEntities())
            {
                db.invoice.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(invoice entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(invoice entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.invoice.Remove(entity);
                db.SaveChanges();
            }
        }
    }
}
