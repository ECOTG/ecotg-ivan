﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class AssetEntity : IEntityCrudWithStringKey<asset>
    {
        public asset GetEntity(string id)
        {
            using (var db = new ecotgEntities())
            {
                return db.asset.FirstOrDefault(e => e.assetId == id);
            }
        }

        public IEnumerable<asset> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.asset select e;
                return entities.ToList();
            }
        }

        public void Insert(asset entity)
        {
            using (var db = new ecotgEntities())
            {
                db.asset.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(asset entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(asset entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.asset.Remove(entity); 
                db.SaveChanges();
            }
        }
    }
}
