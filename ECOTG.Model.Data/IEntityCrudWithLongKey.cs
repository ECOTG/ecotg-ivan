﻿namespace ECOTG.Model.Data
{
    public interface IEntityCrudWithLongKey<T> : IEntityCrud<T>
    {
        T GetEntity(long id);
    }
}
