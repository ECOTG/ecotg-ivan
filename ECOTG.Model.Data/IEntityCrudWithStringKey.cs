﻿namespace ECOTG.Model.Data
{
    public interface IEntityCrudWithStringKey<T> : IEntityCrud<T>
    {
        T GetEntity(string id);
    }
}
