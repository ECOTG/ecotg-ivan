﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Entity;
using ECOTG.Model.Data;
using ECOTG.Helper;

namespace ECOTG.Controller
{
    public static class ClaimController
    {
        //Controller  and the basic CRUD functions, other methods are done here
        public static readonly FormCrudWithStringKeyService<claim> FormCurdService;

        static ClaimController()
        {
            FormCurdService = new FormCrudWithStringKeyService<claim>(new ClaimEntity());
        }

    }
}
