﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class BatchController
    {
        public static readonly FormCrudWithStringKeyService<batch> FormCurdService;

        static BatchController()
        {
            FormCurdService = new FormCrudWithStringKeyService<batch>(new BatchEntity());
        }
    }
}
