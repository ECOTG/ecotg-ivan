﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class CostCentreController
    {
        public static readonly FormCrudWithIntegerKeyService<costcentre> FormCurdService;

        static CostCentreController()
        {
            FormCurdService = new FormCrudWithIntegerKeyService<costcentre>(new CostCentreEntity());
        }
    }
}
