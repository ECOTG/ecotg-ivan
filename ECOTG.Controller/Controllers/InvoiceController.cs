﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class InvoiceController
    {
        public static readonly FormCrudWithIntegerKeyService<invoice> FormCurdService;

        static InvoiceController()
        {
            FormCurdService = new FormCrudWithIntegerKeyService<invoice>(new InvoiceEntity());
        }
    }
}
