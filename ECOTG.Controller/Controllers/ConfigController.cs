﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class ConfigController
    {
        public static readonly FormCrudWithIntegerKeyService<config> FormCrudService;

        static ConfigController()
        {
            FormCrudService = new FormCrudWithIntegerKeyService<config>(new ConfigEntity());
        }
    }
}
