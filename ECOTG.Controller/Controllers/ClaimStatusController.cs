﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class ClaimStatusController
    {
        public static readonly FormCrudWithIntegerKeyService<claimstatus> FormCurdService;

        static ClaimStatusController()
        {
            FormCurdService = new FormCrudWithIntegerKeyService<claimstatus>(new ClaimStatusEntity());
        }

    }
}
