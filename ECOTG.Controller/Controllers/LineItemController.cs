﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class LineItemController
    {
        public static readonly FormCrudWithIntegerKeyService<lineitem> FormCurdService;

        static LineItemController()
        {
            FormCurdService = new FormCrudWithIntegerKeyService<lineitem>(new LineItemEntity());
        }
    }
}
